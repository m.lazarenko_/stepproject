const servicesNav = document.querySelector('.section-nav-list')
const servicesItems = document.querySelectorAll('.services-content')
const loadBtn = document.querySelector('.load-more-btn')
const imagesSection = document.querySelector('.amazing-work-content')
let amazingWorkNav = document.querySelector('.amazing-work-nav-list')
const amazingWorkNavItems = document.querySelectorAll('.amazing-work-nav-item')
const amazingWorkImgs = document.getElementsByClassName('amazing-work-img')
const sliderBar = document.querySelector('.slider-bar')
const nextSliderBtn = document.querySelector('.next')
const prevSliderBtn = document.querySelector('.prev')
const sliderControlBar = document.querySelector('.photos-bar')
const loader = document.querySelector('.loader')

const smallPhotos = document.querySelectorAll('.slider-img')
const masonryGrid = document.querySelector('.masonry-grid')

let photosArr = ['./img/our-amazing-work/1.jpg', './img/our-amazing-work/2.jpg', './img/our-amazing-work/3.jpg', './img/our-amazing-work/4.jpg', './img/our-amazing-work/5.jpg', './img/our-amazing-work/6.jpg', './img/our-amazing-work/7.jpg', './img/our-amazing-work/8.jpg', './img/our-amazing-work/9.jpg', './img/our-amazing-work/10.jpg', './img/our-amazing-work/11.jpg', './img/our-amazing-work/12.jpg', './img/our-amazing-work/13.jpg', './img/our-amazing-work/14.jpg', './img/our-amazing-work/15.jpg', './img/our-amazing-work/16.jpg', './img/our-amazing-work/17.jpg', './img/our-amazing-work/18.jpg', './img/our-amazing-work/19.jpg', './img/our-amazing-work/20.jpg', './img/our-amazing-work/21.jpg', './img/our-amazing-work/22.jpg', './img/our-amazing-work/23.jpg', './img/our-amazing-work/24.jpg'] 

servicesNav.addEventListener('click', (e)=> {
    // переделать на forEach
    let tabId = e.target.getAttribute("data-tab");
    let currentTab = document.querySelector(tabId)


    for(let i = 0; i < servicesNav.children.length; i++){
        servicesNav.children[i].classList.remove('selected-tab')
    }

    servicesItems.forEach(item => {
        item.classList.remove('active')
    })

    e.target.classList.add('selected-tab'); 
    currentTab.classList.add('active')
})


const designBlock = document.querySelector('.creative-design-block')

loadBtn.addEventListener('click', ()=> {    
    loader.toggleAttribute('hidden')
    loadBtn.style.display = 'none'
    setTimeout(()=> {
        loadBtn.style.display = "flex"
        if(amazingWorkImgs.length === 12){
            photosArr.slice(0, 3).forEach(item => {
                let newDiv = document.createElement('div');
                newDiv.classList.add('item');
                newDiv.classList.add('graphic-design');
                let newImg = document.createElement('img');
                newImg.src = item;
                newImg.classList.add('amazing-work-img')
                newDiv.append(newImg);
                let blockClone = designBlock.cloneNode(true)
                newDiv.append(blockClone)
                imagesSection.append(newDiv)
            })
            photosArr.slice(3, 6).forEach(item => {
                let newDiv = document.createElement('div');
                newDiv.classList.add('item');
                newDiv.classList.add('web-design');
                let newImg = document.createElement('img');
                newImg.src = item;
                newImg.classList.add('amazing-work-img')
                newDiv.append(newImg);
                let blockClone = designBlock.cloneNode(true)
                newDiv.append(blockClone)
                imagesSection.append(newDiv)
            })
            photosArr.slice(6, 9).forEach(item => {
                let newDiv = document.createElement('div');
                newDiv.classList.add('item');
                newDiv.classList.add('landing-pages');
                let newImg = document.createElement('img');
                newImg.src = item;
                newImg.classList.add('amazing-work-img')
                newDiv.append(newImg);
                let blockClone = designBlock.cloneNode(true)
                newDiv.append(blockClone)
                imagesSection.append(newDiv)
                
            })
            photosArr.slice(9, 12).forEach(item => {
                let newDiv = document.createElement('div');
                newDiv.classList.add('item');
                newDiv.classList.add('wordpress');
                let newImg = document.createElement('img');
                newImg.src = item;
                newImg.classList.add('amazing-work-img')
                newDiv.append(newImg);
                let blockClone = designBlock.cloneNode(true)
                newDiv.append(blockClone)
                imagesSection.append(newDiv)
            })}
            else if(amazingWorkImgs.length === 24){
                photosArr.slice(12,15).forEach(item => {
                    let newDiv = document.createElement('div');
                    newDiv.classList.add('item');
                    newDiv.classList.add('graphic-design');
                    let newImg = document.createElement('img');
                    newImg.src = item;
                    newImg.classList.add('amazing-work-img')
                    newDiv.append(newImg);
                    let blockClone = designBlock.cloneNode(true)
                    newDiv.append(blockClone)
                    imagesSection.append(newDiv)
                })
                photosArr.slice(15,18).forEach(item => {
                    let newDiv = document.createElement('div');
                    newDiv.classList.add('item');
                    newDiv.classList.add('web-design');
                    let newImg = document.createElement('img');
                    newImg.src = item;
                    newImg.classList.add('amazing-work-img')
                    newDiv.append(newImg);
                    let blockClone = designBlock.cloneNode(true)
                    newDiv.append(blockClone)
                    imagesSection.append(newDiv)
                })
                photosArr.slice(18,21).forEach(item => {
                    let newDiv = document.createElement('div');
                    newDiv.classList.add('item');
                    newDiv.classList.add('landing-pages');
                    let newImg = document.createElement('img');
                    newImg.src = item;
                    newImg.classList.add('amazing-work-img')
                    newDiv.append(newImg);
                    let blockClone = designBlock.cloneNode(true)
                    newDiv.append(blockClone)
                    imagesSection.append(newDiv)
                })
                 photosArr.slice(21,24).forEach(item => {
                    let newDiv = document.createElement('div');
                    newDiv.classList.add('item');
                    newDiv.classList.add('wordpress');
                    let newImg = document.createElement('img');
                    newImg.src = item;
                    newImg.classList.add('amazing-work-img')
                    newDiv.append(newImg);
                    let blockClone = designBlock.cloneNode(true)
                    newDiv.append(blockClone)
                    imagesSection.append(newDiv)
                })        
                       
                    loadBtn.style.display = 'none'
            }
            else {
                console.log(" ERROR : CANT ADD IMAGES")
            }
            contentAnalizer()
    }, 2000)
    
    setTimeout(()=> {
        loader.toggleAttribute('hidden') 
        
    }, 2000)
})


const amazingWorkItems = document.getElementsByClassName('item')

amazingWorkNav.addEventListener('click', (e)=> {
    const targetId = e.target.dataset.id
    amazingWorkNavItems.forEach(item => {
        item.classList.remove('chosen-one')
    })
    switch(targetId) {
        case 'all':
            for(let i = 0; i < amazingWorkItems.length; i++){
                amazingWorkItems[i].style.display = "flex"
            } 
            break;
        case 'graphic-design':
            for(let i = 0; i < amazingWorkItems.length; i++){
                if(amazingWorkItems[i].classList.contains('graphic-design')) {
                    amazingWorkItems[i].style.display = 'flex';
                }
                else {
                    amazingWorkItems[i].style.display = "none";
                }  
            }
            break;
        case 'web-design':
            for(let i = 0; i < amazingWorkItems.length; i++){
                if(amazingWorkItems[i].classList.contains('web-design')) {
                    amazingWorkItems[i].style.display = 'flex';
                }
                else {
                    amazingWorkItems[i].style.display = "none";
                }
            }        
            break;
        case 'landing-pages':
            for(let i = 0; i < amazingWorkItems.length; i++){
                if(amazingWorkItems[i].classList.contains('landing-pages')) {
                    amazingWorkItems[i].style.display = 'flex';
                }
                else {
                    amazingWorkItems[i].style.display = "none";
                }
            }  
            break;  
        case 'wordpress':
            for(let i = 0; i < amazingWorkItems.length; i++){
                if(amazingWorkItems[i].classList.contains('wordpress')) {
                    amazingWorkItems[i].style.display = 'flex';
                }
                else {
                    amazingWorkItems[i].style.display = "none";
                }
            }    
            break;           
    }
    e.target.classList.add('chosen-one')
})




let imgCounter = Math.abs(getComputedStyle(sliderBar).marginLeft.slice(0,5));

nextSliderBtn.addEventListener('click', ()=>{
    console.log(imgCounter)
    if(imgCounter >= 3340){
        imgCounter = 40;
    }
    else {
        imgCounter +=1100
    }
    sliderBar.style.marginLeft = -imgCounter + 'px';
    
    switch (imgCounter) {
        case 3340:
            smallPhotos.forEach(item => {
                item.classList.remove('selected')
            })
            smallPhotos[3].classList.add('selected')
            break;
        case 2240:
            smallPhotos.forEach(item => {
                item.classList.remove('selected')
            })
            smallPhotos[2].classList.add('selected')  
            break;
        case 1140:
            smallPhotos.forEach(item => {
                item.classList.remove('selected')
            })
            smallPhotos[1].classList.add('selected')  
            break;
        case 40:
            smallPhotos.forEach(item => {
                item.classList.remove('selected')
            })
            smallPhotos[0].classList.add('selected') 
            break;     
    }   
})



prevSliderBtn.addEventListener('click', ()=>{
    console.log(imgCounter)
    if(imgCounter <= 40){
        imgCounter = 3340;
    }
    else {
        imgCounter -=1100
    }
    sliderBar.style.marginLeft = -imgCounter + 'px';

    switch (imgCounter) {
        case 3340:
            smallPhotos.forEach(item => {
                item.classList.remove('selected')
            })
            smallPhotos[3].classList.add('selected')
            break;
        case 2240:
            smallPhotos.forEach(item => {
                item.classList.remove('selected')
            })
            smallPhotos[2].classList.add('selected')  
            break;
        case 1140:
            smallPhotos.forEach(item => {
                item.classList.remove('selected')
            })
            smallPhotos[1].classList.add('selected')  
            break;
        case 40:
            smallPhotos.forEach(item => {
                item.classList.remove('selected')
            })
            smallPhotos[0].classList.add('selected') 
            break;  
    }
})



$(window).bind('load', function() {
    jQuery (function($) {
        $(".masonry-grid").masonry({
    
            itemSelector: '.column',
            columns: 3,
            columnWidth: 373, 
            stamp: '.section-title',
            gutter: 20 ,
        })
    })
    
    
    
    jQuery (function($) {
        $(".doubled-img").masonry({
            itemSelector: '.doubled-grid-item',
            columnWidth: ".doubled-grid-item", 
            gutter: 3 ,
        })
    })
    
    jQuery (function($) {
        $(".small-masonry-items").masonry({
            // stamp: '.doubled-img',
            itemSelector: '.small-column',
            columnWidth: ".small-column", 
            gutter: 3
        })
    })
});




smallPhotos.forEach(item => {
    item.addEventListener('click', e => {
        let personCounter


        if(e.target.classList.contains('person4')){
            smallPhotos.forEach(item => {
                item.classList.remove('selected')
            })
            let slider4 = sliderBar.style.marginLeft = -3340 + 'px'
            e.target.classList.add('selected')

                
        }
        else if(e.target.classList.contains('person1')){
            smallPhotos.forEach(item => {
                item.classList.remove('selected')
            })
            let slider1 = sliderBar.style.marginLeft = -40 + 'px'
            e.target.classList.add('selected')
                
        }
        else if(e.target.classList.contains('person2')){
            smallPhotos.forEach(item => {
                item.classList.remove('selected')
            })
            let slider2 = sliderBar.style.marginLeft = -1140 + 'px'
            e.target.classList.add('selected')    
        }
        else if(e.target.classList.contains('person3')){
            smallPhotos.forEach(item => {
                item.classList.remove('selected')
            })
            let slider3 = sliderBar.style.marginLeft = -2240 + 'px'
            e.target.classList.add('selected')
        }
    })
})



const contentTitle = document.getElementsByClassName('creative-design-block-sub-title')

function contentAnalizer(){
    for(let i = 0; i < contentTitle.length; i++){
        if(contentTitle[i].closest('.item').classList.contains('graphic-design')){
            contentTitle[i].textContent = 'Graphic Design'
        }
        else if (contentTitle[i].closest('.item').classList.contains('web-design')) {
            contentTitle[i].textContent = 'Web Design'
        }
        else if (contentTitle[i].closest('.item').classList.contains('landing-pages')) {
            contentTitle[i].textContent = 'Landing Pages'
        }
        else {
            contentTitle[i].textContent = "Wordpress"
        }
    }
}

contentAnalizer()

